# -*- coding: utf-8 -*-

import abc
import functools
import json
import random
import time

import redis


def retry(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        self = args[0]
        attempts = self.retry
        backoff_factor = self.backoff_factor
        attempt = 0
        while attempt < attempts:
            try:
                return func(*args, **kwargs)
            except self.__class__.Exception:
                attempt += 1
                if attempt >= attempts:
                    raise
                delay = backoff_factor * (2 ** attempt)
                time.sleep(delay)

    return wrapper


class AbstractStore(object):
    __metaclass__ = abc.ABCMeta

    Exception = Exception

    def __init__(self, connection, retry=5, backoff_factor=0):
        self.connection = connection
        self.retry = retry
        self.backoff_factor = backoff_factor

    @abc.abstractmethod
    def get(self, key):
        pass

    @abc.abstractmethod
    def set(self, key, value, ttl=None):
        pass

    def cache_set(self, key, value, ttl):
        try:
            return self.set(key, value, ttl)
        except self.__class__.Exception:
            return None

    def cache_get(self, key):
        try:
            return self.get(key)
        except self.__class__.Exception:
            return None


class RedisStore(AbstractStore):
    Exception = redis.exceptions.RedisError

    @retry
    def get(self, key):
        return self.connection.get(key)

    @retry
    def set(self, key, value, ttl=None):
        return self.connection.set(key, value)


class FakeStore(AbstractStore):
    storage = {}
    interests = ["cars", "pets", "travel", "hi-tech", "sport", "music", "books", "tv", "cinema", "geek", "otus"]

    def get(self, key):
        if key.startswith('i:'):
            return json.dumps(random.sample(self.interests, random.randint(1, 3)))
        else:
            return self.storage.get(key, None)

    def set(self, key, value, ttl=None):
        self.__class__.storage[key] = value
