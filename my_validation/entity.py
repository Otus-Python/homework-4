# -*- coding: utf-8 -*-
import copy
import numbers

from .fields import SimpleField


class EntityMeta(type):
    """Metaclass for business entities with validated fields"""

    def __init__(cls, name, bases, attr_dict):
        cls.required_fields = []
        super(EntityMeta, cls).__init__(name, bases, attr_dict)
        for key, attr in attr_dict.items():
            if isinstance(attr, SimpleField):
                if attr.required:
                    cls.required_fields.append(key)
                type_name = type(attr).__name__
                attr.storage_name = '_{}#{}'.format(type_name, key)


class Entity(object):
    """Business entity with validated fields"""

    __metaclass__ = EntityMeta

    def __init__(self, json):
        self._has = []
        self._errors = []
        self._data = copy.copy(json)
        self._validate()
        self._init_fields()

    def _init_fields(self):
        for k, v in self._data.iteritems():
            try:
                setattr(self, k, v)
                if v or isinstance(v, numbers.Integral):
                    self._has.append(k)
            except ValueError as e:
                self._errors.append('"{}" {!s}'.format(k, e))

    def _validate_required_fields(self):
        for field in self.required_fields:
            if field not in self._data:
                self._errors.append('Request must contain attribute "{}"'.format(field))

    def _validate(self):
        """
        метод, который мы будем переопределять
        если нужна дополнительная логика при валидации в дочерних классах
        :return:
        """
        self._validate_required_fields()

    @property
    def has(self):
        return self._has[:]

    @property
    def errors(self):
        return self._errors[:]
