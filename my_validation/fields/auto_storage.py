# -*- coding: utf-8 -*-
import abc


class AutoStorage(object):
    """
    Дескрипторный класс, который автоматически управляет атрибутами хранения
    """
    __counter = 0

    def __init__(self):
        cls = self.__class__
        prefix = cls.__name__
        index = cls.__counter
        self.storage_name = '_{}#{}'.format(prefix, index)
        cls.__counter += 1

    def __get__(self, instance, owner):
        """
        если обращение к свойству осуществляется от имени класса, то возвращается сам дескриптор
        иначе возвращается значение замаскированного дескриптором атрибута

        :param instance:
        :param owner:
        :return:
        """
        if instance is None:
            return self
        else:
            return getattr(instance, self.storage_name, None)

    def __set__(self, instance, value):
        setattr(instance, self.storage_name, value)


class Validated(AutoStorage):
    """
    абстрактный класс, реализующий паттерн шаблонный метод.
    переопределяется метод __set__, в нем осуществляется вызов абстрактного метода validate
    который должен реализовавыться в дочерних классах
    """
    __metaclass__ = abc.ABCMeta

    def __set__(self, instance, value):
        value = self.validate(instance, value)
        super(Validated, self).__set__(instance, value)

    @abc.abstractmethod
    def validate(self, instance, value):
        """return validated value or raise ValueError"""
