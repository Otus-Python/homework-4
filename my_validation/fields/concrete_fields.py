# -*- coding: utf-8 -*-
import datetime
import numbers

from . import Validated as AbstractField

AGE_LIMIT = 70

UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class SimpleField(AbstractField):
    """
    базовый неабстрактный филд
    """

    def __init__(self, required=True, nullable=True):
        super(SimpleField, self).__init__()
        self.required = required
        self.nullable = nullable

    def validate(self, instance, value):
        if value is None and not self.nullable:
            raise ValueError("value must not be None")
        return value


class CharField(SimpleField):
    def validate(self, instance, value):
        value = super(CharField, self).validate(instance, value)
        if not isinstance(value, basestring):
            raise ValueError("value must be a string")
        return value


class ArgumentsField(SimpleField):
    def validate(self, instance, value):
        value = super(ArgumentsField, self).validate(instance, value)
        if not isinstance(value, dict):
            raise ValueError("value must be an object")
        return value


class EmailField(CharField):
    def validate(self, instance, value):
        value = super(EmailField, self).validate(instance, value)
        if '@' not in value:
            raise ValueError("value must be a valid email address")
        return value


class PhoneField(SimpleField):
    def validate(self, instance, value):
        value = str(super(PhoneField, self).validate(instance, value))
        if not (len(value) == 11 and value.startswith('7')):
            raise ValueError("value must contain exactly 11 characters and start with 7")
        return value


class DateField(SimpleField):
    def validate(self, instance, value):
        value = super(DateField, self).validate(instance, value)
        try:
            value = datetime.datetime.strptime(value, '%d.%m.%Y')
        except ValueError:
            raise ValueError('value must be a date in DD.MM.YYYY format')
        return value


class BirthdayField(DateField):
    def validate(self, instance, value):
        value = super(BirthdayField, self).validate(instance, value)
        today = datetime.datetime.now()
        age = today.year - value.year - ((today.month, today.day) < (value.month, value.day))
        if age > AGE_LIMIT:
            raise ValueError('age must be less than {} years'.format(AGE_LIMIT))
        return value


class GenderField(SimpleField):
    def validate(self, instance, value):
        value = super(GenderField, self).validate(instance, value)
        if value not in GENDERS:
            raise ValueError("value must be from set ({})".format(", ".join(str(i) for i in GENDERS)))
        return value


class ClientIDsField(SimpleField):
    def validate(self, instance, value):
        value = super(ClientIDsField, self).validate(instance, value)
        if not (isinstance(value, list) and len(value) and all(isinstance(id_, numbers.Integral) for id_ in value)):
            raise ValueError("value must be a non empty list of int numbers")
        return value
