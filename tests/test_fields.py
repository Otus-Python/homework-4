# -*- coding: utf-8 -*-

"""
I don't know the right way for testing descriptors,
so I just do what recommends stackoverflow

https://stackoverflow.com/questions/22843554/in-python-how-can-i-call-an-instance-method-on-a-descriptor/22843653#22843653

"""
import datetime
import unittest

import my_validation.fields as fields
from tests import cases


class MockClass(object):
    pass


class TestCase(unittest.TestCase):
    """
    I avoid setUp repetition
    """

    def setUp(self):
        self.instance = MockClass()


class TestSimpleField(TestCase):

    def test_set_none_into_not_nullable_field(self):
        descriptor = fields.SimpleField(nullable=False)
        with self.assertRaisesRegexp(ValueError, 'must not be None'):
            descriptor.__set__(self.instance, None)

    @cases([0, [], {}, False])
    def test_set_falthy_into_not_nullable_field(self, value):
        descriptor = fields.SimpleField(nullable=False)
        descriptor.__set__(self.instance, value)
        self.assertIs(descriptor.__get__(self.instance, MockClass), value)

    @cases([0, [], {}, None, False, "Hello world", 25, {'key': 'value'}, [1, 2, 3]])
    def test_set_values_into_nullable_field(self, value):
        descriptor = fields.SimpleField(nullable=True)
        descriptor.__set__(self.instance, value)
        self.assertIs(descriptor.__get__(self.instance, MockClass), value)


class TestCharField(TestCase):

    @cases([{}, [], 0, 123, 12.5])
    def test_set_not_string(self, value):
        descriptor = fields.CharField(nullable=False)
        with self.assertRaisesRegexp(ValueError, 'must be a string'):
            descriptor.__set__(self.instance, value)

    @cases(['', u'', 'string', u'строка', '123', u'123.5'])
    def test_set_string(self, value):
        descriptor = fields.CharField(nullable=True)
        descriptor.__set__(self.instance, value)
        self.assertIs(descriptor.__get__(self.instance, MockClass), value)


class TestEmailField(TestCase):
    """
    email ‐ строка, в которой есть @
    """

    @cases(['qwerty'])
    def test_set_email_without_at_sign(self, value):
        descriptor = fields.EmailField(nullable=False)
        with self.assertRaisesRegexp(ValueError, 'must be a valid email'):
            descriptor.__set__(self.instance, value)

    @cases(['qwe@rty', u'@', 'test@mail.ru'])
    def test_set_valid_email(self, value):
        descriptor = fields.EmailField(nullable=True)
        descriptor.__set__(self.instance, value)
        self.assertIs(descriptor.__get__(self.instance, MockClass), value)


class TestArgumentsField(TestCase):

    @cases([[], 123, type('NewClass', (object,), {}), 'qwerty'])
    def test_set_not_dict(self, value):
        descriptor = fields.ArgumentsField(nullable=False)
        with self.assertRaisesRegexp(ValueError, 'must be an object'):
            descriptor.__set__(self.instance, value)

    @cases([
        {}, {'key': 'value'}, {'a': [x for x in xrange(5)]}
    ])
    def test_set_dict(self, value):
        descriptor = fields.ArgumentsField(nullable=True)
        descriptor.__set__(self.instance, value)
        self.assertIs(descriptor.__get__(self.instance, MockClass), value)


class TestPhoneField(TestCase):
    """
    строка или число, длиной 11, начинается с 7
    """

    @cases([['89175002040', 89175002040, '+79175002040', '8 917 500-20-40']])
    def test_set_invalid_phone(self, value):
        descriptor = fields.PhoneField(nullable=False)
        with self.assertRaisesRegexp(ValueError, 'must contain exactly 11 characters and start with 7'):
            descriptor.__set__(self.instance, value)

    @cases(['79175002040', 79175002040])
    def test_set_valid_phone(self, value):
        descriptor = fields.PhoneField(nullable=True)
        descriptor.__set__(self.instance, value)
        self.assertEqual(descriptor.__get__(self.instance, MockClass), str(value))


class TestDateField(TestCase):
    """
    дата в формате DD.MM.YYYY
    """

    @cases(['2017.12.31', '2017.31.12', '12.31.2017', '30.02.2017'])
    def test_set_invalid_date(self, value):
        descriptor = fields.DateField(nullable=False)
        with self.assertRaisesRegexp(ValueError, 'must be a date in DD.MM.YYYY format'):
            descriptor.__set__(self.instance, value)

    @cases([
        {'value': '31.12.2017', 'date': datetime.datetime(year=2017, month=12, day=31)},
        {'value': '01.01.2018', 'date': datetime.datetime(year=2018, month=1, day=1)},
    ])
    def test_set_valid_date(self, arguments):
        descriptor = fields.DateField(nullable=False)
        descriptor.__set__(self.instance, arguments['value'])
        self.assertEqual(descriptor.__get__(self.instance, MockClass), arguments['date'])


class TestBirthdayField(TestCase):
    """
    birthday ‐ дата в формате DD.MM.YYYY, с которой прошло не больше 70 лет
    """

    def test_too_old_birthday(self):
        descriptor = fields.BirthdayField(nullable=False)
        with self.assertRaisesRegexp(ValueError, 'age must be less than'):
            descriptor.__set__(self.instance, '25.01.1938')

    @cases([
        (datetime.datetime.now() - datetime.timedelta(days=365 * 70)).strftime('%d.%m.%Y'),  # old, but less than 70 yo
        (datetime.datetime.now() - datetime.timedelta(days=365 * 30)).strftime('%d.%m.%Y'),
        datetime.datetime.now().strftime('%d.%m.%Y'),
    ])
    def test_set_valid_birthday(self, birthday):
        descriptor = fields.BirthdayField(nullable=False)
        descriptor.__set__(self.instance, birthday)
        self.assertEqual(descriptor.__get__(self.instance, MockClass), datetime.datetime.strptime(birthday, '%d.%m.%Y'))


class TestGenderField(TestCase):
    """
    gender ‐ число 0, 1 или 2
    """

    @cases([-1, 3, 'male'])
    def test_set_invalid_gender(self, value):
        descriptor = fields.GenderField(nullable=False)
        with self.assertRaisesRegexp(ValueError, 'value must be from set \('):
            descriptor.__set__(self.instance, value)

    @cases([0, 1, 2])
    def test_set_valid_gender(self, value):
        descriptor = fields.GenderField(nullable=True)
        descriptor.__set__(self.instance, value)
        self.assertIs(descriptor.__get__(self.instance, MockClass), value)


class TestClientIdsField(TestCase):
    @cases([[], {}, {'key': 'value'}, 'string', 123])
    def test_set_invalid_id_list(self, value):
        descriptor = fields.ClientIDsField(nullable=False)
        with self.assertRaisesRegexp(ValueError, 'must be a non empty list of int numbers'):
            descriptor.__set__(self.instance, value)

    @cases([[1], [0, 1, 2, 3]])
    def test_set_valid_id_list(self, value):
        descriptor = fields.ClientIDsField(nullable=True)
        descriptor.__set__(self.instance, value)
        self.assertIs(descriptor.__get__(self.instance, MockClass), value)


if __name__ == "__main__":
    unittest.main()
