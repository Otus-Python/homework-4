# -*- coding: utf-8 -*-

import unittest

import mock
import redis.exceptions as rexceptions

from store import RedisStore
from tests import cases


class TestRedisStore(unittest.TestCase):

    def setUp(self):
        redis_mock = mock.MagicMock()
        redis_mock.set = mock.MagicMock()
        redis_mock.get = mock.MagicMock()
        self.redis = redis_mock
        self.store = RedisStore(redis_mock, retry=5, backoff_factor=0)

    @cases([None, '1.0', '2.0', '3.0'])
    def test_redis_is_ok(self, value):
        self.redis.get.return_value = value
        self.assertEqual(self.store.cache_get('key'), value)

    def test_retry_is_work(self):
        self.redis.get.side_effect = [rexceptions.TimeoutError, rexceptions.ConnectionError, '4.0']
        self.assertEqual(self.store.cache_get('key'), '4.0')
        self.assertEqual(self.redis.get.call_count, 3)

    def test_cache_get_returns_none_if_redis_is_dead(self):
        self.redis.get.side_effect = [
            rexceptions.ConnectionError,
            rexceptions.ConnectionError,
            rexceptions.ConnectionError,
            rexceptions.ConnectionError,
            rexceptions.ConnectionError,
        ]
        self.assertEqual(self.store.cache_get('key'), None)
        self.assertEqual(self.redis.get.call_count, 5)

    def test_get_raise_exception_if_redis_is_dead(self):
        self.redis.get.side_effect = [
            rexceptions.ConnectionError, rexceptions.ConnectionError,
            rexceptions.ConnectionError, rexceptions.ConnectionError,
            rexceptions.ConnectionError,
        ]
        with self.assertRaises(rexceptions.ConnectionError):
            self.store.get('key')
        self.assertEqual(self.redis.get.call_count, 5)

    def test_cache_set_returns_none_if_redis_is_dead(self):
        self.redis.set.side_effect = [
            rexceptions.ConnectionError, rexceptions.ConnectionError,
            rexceptions.ConnectionError, rexceptions.ConnectionError,
            rexceptions.ConnectionError,
        ]

        self.assertEqual(self.store.cache_set('key', 'value', 20), None)
        self.assertEqual(self.redis.set.call_count, 5)

    def test_set_raise_exception_if_redis_is_dead(self):
        self.redis.set.side_effect = [
            rexceptions.ConnectionError, rexceptions.ConnectionError,
            rexceptions.ConnectionError, rexceptions.ConnectionError,
            rexceptions.ConnectionError,
        ]
        with self.assertRaises(rexceptions.ConnectionError):
            self.store.set('key', 'value', 20)
        self.assertEqual(self.redis.set.call_count, 5)


if __name__ == "__main__":
    unittest.main()